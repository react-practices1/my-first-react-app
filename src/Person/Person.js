import React from 'react';
import styled from 'styled-components';

// import './Person.css';
const StyleDiv = styled.div`
    width: 60%;
    margin: 16px auto;
    border: 1px solid #eee;
    box-shadow: 0 2px 3px #ccc;
    padding: 16px;
    text-align: center;

    @media (min-width: 500px){
        width: 450px;
    }
}
`
const person = (props) => {
    // const style = {
    //     '@media (min-width: 500px)': {
    //         width: '450px'
    //     }
    // }

    const rnd = Math.random();
    if(rnd>0.7){
        throw new Error('Algo salió mal!');
    }
    return (
        <StyleDiv>
            <p onClick={props.click}>Soy {props.name} y tengo {props.age} años!</p>
            <p>{props.children}</p>
            <input type="text" value={props.name} onChange={props.changed}/>
        </StyleDiv>
    )
}

export default person;