import React, {Component} from 'react';
import styled from 'styled-components';
import './App.css';
import Person from './Person/Person';
import ErrorBoundary from './ErrorBoundary/ErrorBoundary';

const StyledButton = styled.button`
  background-color: ${props => props.alt ? 'red' : 'green'};
  color: white;
  front: inherit;
  border: 1px solid blue;
  padding: 8px;
  cursor: pointer;

  &:hover {
    background-color: ${props => props.alt ? 'salmon' : 'lightgreen'};
    color: black;
  }
`;

class App extends Component{
  state = {
    persons:[
      { id:'1adsgfg', name:'Lucas', age: 32 },
      { id:'2aadsds', name:'Matias', age: 33 },
      { id:'3aasdds', name:'Martin', age: 30 }
    ],
    otherState: 'some other value',
    showPersons: false
  };

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = { ...this.state.persons[personIndex] };
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState( { persons: persons } );
    
  };

  deletePersonHandler = (personIndex) => {
    // const persons = this.state.persons;
    const persons = [...this.state.persons]
    persons.splice(personIndex,1);
    this.setState({persons: persons})
  };

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow}); 
  };

  render(){
    const style = {
      backgroundColor: 'green',
      front: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      ':hover': {
        backgroundColor: 'lightgreen',
        color: 'black'
      }
    };

    let persons = null;

    if (this.state.showPersons){
      persons = (
        <div> 
          {this.state.persons.map((person, index) => {
            return <ErrorBoundary key={person.id}> 
            <Person 
              click={() => this.deletePersonHandler(index)}
              name={person.name}
              age={person.age}
              key={person.id}
              changed={(event) => this.nameChangedHandler(event,person.id)}  /> 
            </ErrorBoundary>
          })}
        </div>
      );

      // style.backgroundColor = 'red';
      // style[':hover'] = {
      //   backgroundColor: 'salmon',
      //   color: 'white'
      // }
    }

    let classes = [];
    if(this.state.persons.length <= 2){
      classes.push('red'); // classes = ['red']
    }
    if(this.state.persons.length <= 1){
      classes.push('bold'); // classes = ['red','bold']
    }

    return (
        <div className="App">
          <h1>Hola, soy la App!</h1>
          <p className={classes.join(' ')}>Soy el parrafo!</p> 
          <StyledButton alt={this.state.showPersons} onClick={this.togglePersonsHandler}>
              Toggle Name
          </StyledButton>
          {persons}
        </div>
    );
  }
}

export default App;
